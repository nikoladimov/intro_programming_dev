#include <iostream>
#include <string>

using namespace std;

/* Exercise 1
bool compare(string& str_in1, string& str_in2)
{
    for(int i=0; i<str_in1.length(); i++)
    {
        if(tolower(str_in1[i]) == tolower(str_in2[i]))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}

int main()
{
    string str_in1, str_in2;

    while(cin >> str_in1 >> str_in2)
    {
        if(str_in1.length() != str_in2.length())
        {
            cout << "NO";
        }
        else
        {
            if(compare(str_in1, str_in2) == true)
            {
                cout << "YES";
            }
            else
            {
                cout << "NO";
            }
        }
    }
}
*/

string encrypt(int key, string txt)
{
    string newStr;

    //cycle to go through each symbol of the inputed string
    for(int i=0; i<txt.length(); i++)
    {
        //if the symbol is lowercase letter => use key to encrypt to different lowercase letter
        if((int)txt[i]>96 && (int)txt[i]<123)
        {
            newStr += char(97 + (txt[i] + key - 97) % 26);
        }
        //if the symbol is uppercase letter => use key to encrypt to different uppercase letter
        else if((int)txt[i]>64 && (int)txt[i]<91)
        {
            newStr += char(65 + (txt[i] + key - 65) % 26);
        }
        //if the symbol is different than letter => add it to the new string
        else
        {
            newStr += txt[i];
        }
    }

    return newStr;
}

int main()
{
    int key;
    string txt;

    cin >> key;

    while(getline(cin, txt))
    {
        cout << encrypt(key, txt);
    }

}
