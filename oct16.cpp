#include <string>
#include <iostream>

using namespace std; //instead of writing std:: in front of everything

int main() {
    string yourName;
    string yourSurname;

    cout << "Please enter your name: ";
    cin >> yourName;

    cout << "Please enter your surname: ";
    cin >> yourSurname;


    cout << "Hello ";
    cout << yourName; //std::cout << yourName;
    cout << " ";
    cout << yourSurname;
    cout << "!";
    return 0;
}
