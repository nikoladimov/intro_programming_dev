#include <iostream>
#include <string>

using namespace std;

string only_lowercase(string str)
{
    string str_new="";

    for(int i=0; i<str.length(); i++)
    {
        if((int)str[i]==32 || (int)str[i]>96 && (int)str[i]<123)
        {
            str_new+=str[i];
        }
    }

    return str_new;
}

int main()
{
    string str = "Az obicham da rabotq";

    cout << only_lowercase(str);
}
