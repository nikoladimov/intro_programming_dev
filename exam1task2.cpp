//Exercise 2
//Faculty Number: F75329
//Author: Nikola Dimov

#include <iostream>

using namespace std;

int main ()
{
    float shares, percent;
    long total;
    const long TOTAL_ASSETS=72000000;

    cout << "Enter your ownership in KTB:";
    cin >> shares;

    if(shares<0)
    {
        cout << "You can't own less than 0%";
    }
    else if (shares>1)
    {
        cout << "You can't own more than 100%";
    }
    else
    {
        percent=shares*100;
        total=shares*TOTAL_ASSETS;
        cout << "You have " << percent << "% shares or " << total << " BGN." << endl;
    }

    return 0;
}
