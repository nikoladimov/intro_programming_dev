/***
FN:F75329
PID:3
GID:2
*/

/*�������� ������������ ��������, ����� �� ���������� 1 ������. �� ������ �� ������� �� ����������� ���� 1 ����� N - ���� �� ���������� �� ������� � N �� �� ���� ����� - ���� �� �������.
���� ����� ������� �� ���� � ��������� �������� � ���������� ������. ���������� ������ �� ��������� �������� ��������:
a) add 2 -4 - ������ ������� -4 �� ������� 2 ��� ������� (���� �� ���������, �� ��������� �� ����� � ������ �� �������)
�) remove 2 - �������� ������� �� ������� 2 ��� ������� (���� �� ���������, �� ��������� �� ����� � ������ �� �������)
�) shift_left - ���������� �������� ������ �������� � 1 ������� �� ���� ���� ���������� ������� � �������� ������� �� ����������. ��������, ��� ������� ��� ������� �� "1, 2, 3", ���� ���������� ������� ��� ������� �� �����: "2, 3, 1"
�) reverse - ������ ������� ������ �������� ���� ���������� ������� � �������� ������� �� ����������. ��������, ��� ������� ��� ������� �� "1, 2, 3", ���� ���������� ������� ��� ������� �� �����: "3, 2, 1". �� ���� �� �������� ������ ������� �� �������� �� ������, ������ �� �������������� ���� �������������� ����.
�) print - ��������� �������� ��������� �� ���������� �� �������
�) exit - �������� ������������ �� ����������*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

int position = 0;

void vectorLeftShift(vector <double> &A)
{
    double firstValue = A[0];

    for(position = 0; position < A.size()-1; position++)
    {
        A[position] = A[position+1];
    }

    A[A.size()-1] = firstValue;
}

void vectorReverse(vector <double> &A)
{
    double tempValue;

    for(position = 0; position < A.size()-2; position++)
    {
        tempValue = A[position];
        A[position] = A[A.size()-position-1];
        A[A.size()-position-1] = tempValue;
    }
}

int main()
{
	int vectorSize;

    cin >> vectorSize;

	vector <double> A(vectorSize);

    while(position < vectorSize)
    {
        cin >> A[position];
        position++;
    }

    string command = "";
    double value;

    while(command != "exit")
    {
        cin >> command;

        if(command == "add")
        {
            cin >> position >> value;
            A.insert(A.begin()+position-1, value);
        }
        else if(command == "remove")
        {
            cin >> position;
            A.erase(A.begin()+position-1);
        }
        else if(command == "shift_left")
        {
            vectorLeftShift(A);
        }
        else if(command == "reverse")
        {
            vectorReverse(A);
        }
        else if(command == "print")
        {
            position = 0;

            while(position < A.size())
            {
                cout << A[position] << " ";
                position++;
            }

            cout << endl;
        }
    }

	return 0;
}
