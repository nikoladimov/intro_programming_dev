//Exercise 1
//Faculty Number: F75329
//Author: Nikola Dimov

#include <iostream>
#include <string>

using namespace std;

int main ()
{
    string fname, lname;

    cout << "Enter your name:";
    cin >> fname >> lname;

    cout << lname << ", " << fname;

    return 0;
}
