/***
FN:F75329
PID:2
GID:2
*/

/*�������� ��������, ����� ���� �� �������� �������� ������ �� �����. ���������� ������ �� ���� �� ���� �� ����� ���������� �� �������.
������������ ���������� � �������, ����� ������ ������� �� ���������� � �� ����� ��������. ���� ������������ �� ��������� ��������� ������ ������ �� � �������� �������� �� ����� - a[0]->a[1], a[1]->a[2], ..., a[n-1]->a[0].*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

int position = 0;

void vectorRightShift(vector <double> &A)
{
    double lastValue = A[A.size()-1];

    for(position = A.size() - 1; position > 0; position--)
    {
        A[position] = A[position-1];
    }

    A[0] = lastValue;
}

int main()
{
    double vectorValue;

	vector <double> A;

    //!���������� ���� �� ���� �� ����� ���������� �� �������, �.�. �� ��������� �� ��������, ����������� �� ������
	while(cin >> vectorValue)
    {
        A.push_back(vectorValue);
    }

    vectorRightShift(A);

    while(position < A.size())
    {
        cout << A[position] << " ";
        position++;
    }

	return 0;
}
