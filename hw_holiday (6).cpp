#include <iostream>
#include <string>

using namespace std;

int main()
{
    double num, minN=2000000, maxN=-2000000, sumN=0, countN=0, avgN=0;

    while(cin >> num)
    {
        minN = min(minN, num);
        maxN = max(maxN, num);
        sumN += num;
        countN ++;
        avgN = sumN / countN;
    }

    cout << "\nMin: " << minN  << "\nMax: " << maxN << "\nSum: " << sumN << "\nCount: " << countN << "\nAverage: " << avgN << endl;
}
