#include <iostream>
#include <string>

using namespace std;

string str_check(string shablon, string str)
{
    if(shablon[0] == '*')
    {
        if(str.substr(str.length()-shablon.length()+1, str.length()-1) == shablon.substr(1, shablon.length()-1))
        {
            return "YES\n";
        }
        else
        {
            return "NO\n";
        }
    }
    else if(shablon[shablon.length()-1] == '*')
    {
        if(str.substr(0, shablon.length()-2) == shablon.substr(0, shablon.length()-2))
        {
            return "YES\n";
        }
        else
        {
            return "NO\n";
        }
    }
}

int main()
{
    string shablon, str;

    cin >> shablon;

    cin.ignore();

    while(getline(cin,str))
    {
        cout << str_check(shablon, str);
    }
}
