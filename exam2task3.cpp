#include <iostream>
#include <string>

using namespace std;

string upp(string& in_str)
{
    string n;
    string new_str="";

    for(int i=0; i<in_str.length(); i++)
    {
        n=in_str.substr(i, 1);

        if(isupper(n[0]))
        {
            new_str.append(n);
        }
    }

    return new_str;
}

int main()
{
    int num_str;
    string in_str;

    cin>>num_str;

    if(num_str>0)
    {
        for(int i=1; i<=num_str; i++)
        {
            cin>>in_str;
            cout<<upp(in_str);
        }
    }
    else
    {
        return 0;
    }
}
