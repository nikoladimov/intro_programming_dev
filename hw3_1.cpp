/***
FN:F75329
PID:1
GID:2
*/

/*�������� ��������, ����� ��������� �������� ����������� �� ��� ������� - A � B. �������� ������������ �: A[0]*B[0] - A[1]*B[1] - A[2]*B[2] - ... - A[n-1]*B[n-1].
���������� ������ �� ���� �� ����������� ���� ���� ����� N - ���� �� ���������� �� ���������. ������� N �� �� ���� ����� - ���� �� ������ ������, ���� ����� ��� N ����� - ���� �� ������.
������������ ���������� � �������, ����� ������ 2 ������� � ����� double.*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

int position = 0;

double happyMultiplication(vector <double> A, vector <double> B)
{
    double result = A[0]*B[0];

    for(position = 1; position < A.size(); position++)
    {
        result -= A[position]*B[position];
    }

    return result;
}

int main()
{
    int vectorSize;

    cin >> vectorSize;

	vector <double> A(vectorSize);
    vector <double> B(vectorSize);

    while(position < vectorSize)
    {
        cin >> A[position];
        position++;
    }

    position = 0;

    while(position < vectorSize)
    {
        cin >> B[position];
        position++;
    }

    cout << happyMultiplication(A, B);

	return 0;
}
