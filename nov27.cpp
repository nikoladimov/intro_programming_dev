#include <iostream>

#include <string>

#include <iomanip>

using namespace std;

/*Exercise 1
bool isprime(int a)
{
    for(int i=2; i<a; i++)
    {
        if(a%i==0)
        {
            return false;
        }
    }
    return true;
}

int main()
{
    int a;

    while(cin>>a)
    {
       bool result=isprime(a);

       if(a<2)
        {
            cout<<"N/A"<<endl;
        }
        else if(result)
        {
            cout<<"YES"<<endl;
        }
        else
        {
            cout<<"NO"<<endl;
        }
    }
}*/

/*Exercise 2
void modify(string& str)
{
    string x(str.rbegin(), str.rend());
    str=x;

}

int main()
{
    string str;

    cin>>str;


    if(str=="exit")
    {
        return 0;
    }
    else
    {
        modify(str);
        cout<<str;
    }
}*/

/*Exercise 3
bool isgood(int a)
{
    int b;

    while(a>0)
    {
        b=a%10;
        if(b%2!=0)
        {
            return false;
        }
        a=a/10;
    }

    return true;
}

int main()
{
    int a;

    while(cin>>a)
    {
        bool result=isgood(a);

        if(a<2)
        {
            cout<<"N/A"<<endl;
        }
        else if(result)
        {
            cout<<"YES"<<endl;
        }
        else
        {
            cout<<"NO"<<endl;
        }
    }
}*/

//Exercise 4 previous lecture

int main()
{
    string f;
    double a, b, c, d;

    cin >> f >> a >> b >> c >> d;

    cout << setw(10) << "Faculty No" << setw(6) << "G1" << setw(6) <<"G2" << setw(6) <<"G3" << setw(6) <<"G4"<< endl;
    cout << setw(10) << f << setw(6) << setw(6) <<a << setw(6) <<b << setw(6) <<c << setw(6) <<d;
}
