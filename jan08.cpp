#include <iostream>
#include <vector> //include biblioteka vector

using namespace std;

/*int main()
{
    int A[6]; //6 elementa s pozicii ot 0 do 5
    //sizeof kolko elementa ima v masiva
    //vector na koito mu kzvame s kakuv tip danni shte raboti
    vector <int> A;
    A[0]=2; // crash zashtoto A ima 0 elementa

    vector <int> A(6); //suzdava masiv s pozicii ot 0 do 5 => mojem da zadavame stoinosti na vsqka poziciq A[n]=...
    //A.size(); --- vrushta duljinata na  masiva

    .erase // trie poziciq ot masiva
}*/

double sum(vector <double> A)
{
    double sum=0;

    for(int i=0; i<A.size(); i++)
    {
        sum+=A[i];
    }

    return sum;
}

double minN(vector <double> A)
{
    double minN=A[0];

    for(int i=1; i<A.size(); i++)
    {
        if(A[i]<minN)
        {
            minN = A[i];
        }
    }

    return minN;
}

double maxN(vector <double> A)
{
    double maxN=A[0];

    for(int i=1; i<A.size(); i++)
    {
        if(A[i]>maxN)
        {
            maxN = A[i];
        }
    }

    return maxN;
}

int main()
{
    int num_read, i=0, countN;
    string cmd;

    cin >> num_read;

    vector <double> A(num_read);

    while(i<num_read)
    {
        cin >> A[i];
        i++;
    }

    while(cin >> cmd)
    {
        if(cmd == "sum")
        {
            cout << sum(A);
        }
        else if(cmd == "avg")
        {
            cout << sum(A)/A.size();
        }
        else if(cmd == "min")
        {
            cout << minN(A);
        }
        else if(cmd == "max")
        {
            cout << maxN(A);
        }
        else if(cmd == "ins")
        {
            cin >> A.push_back();
        }
    }
}
