#include <iostream>
#include <algotithm>

using namespace std;

int main()
{
    double num;
    double minN=2000000;
    double maxN=-2000000;


    while(cin>>num)
    {
        if(num>maxN)
        {
            maxN=min(minN,num);
        }
        if (num<minN)
        {
            minN=num;
        }
    }

    cout << minN << " " << maxN;
}
