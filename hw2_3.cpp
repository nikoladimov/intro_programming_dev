/*�������� ��������, ����� ���� (�� ���� �� �����) ���������� ����� � ����� (����� ����� 2.0 � 6.0). �������� �������, ����� ������ ���� ��������� ����� ��������� � ���������� ����������� ����� ���� ������ ��� ���� ��������� (� C++ ���� � ������� '\t') � �������� ������������� � ������� A-F. ��������� �������� ������:

� - ��� 5.50

B - �� 4.50 �� 5.49

C - �� 3.50 �� 4.49

D - �� 3.00 �� 3.49

E - �� 2.50 �� 2.99

F - �� 2.49*/

#include <iostream>
#include <string>

using namespace std;

void fNum_modify(string& fNum)
{
    fNum.append("\t");
}

string mark_modify(float mark)
{
    if(mark>5.5)
    {
        return "A";
    }
    else if(mark>4.5)
    {
        return "B";
    }
    else if(mark>3.5)
    {
        return "C";
    }
    else if(mark>3)
    {
        return "D";
    }
    else if(mark>2.5)
    {
        return "E";
    }
    else
    {
        return "F";
    }
}

int main()
{
    string fNum, m;
    float mark;

    while(cin>>fNum>>mark)
    {
        if(mark<0 or mark>6)
        {
            return 0;
        }
        else
        {
            fNum_modify(fNum);
            cout<<fNum<<mark_modify(mark)<<endl;
        }
    }
}
