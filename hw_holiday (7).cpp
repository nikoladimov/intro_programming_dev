#include <iostream>
#include <string>

using namespace std;

int main()
{
    string cmd;
    double check=0, num, sumN=0, minN=2000000, maxN=-2000000;

    while(cin >> cmd)
    {
        if(cmd == "add" || cmd == "min" || cmd == "max" || cmd == "sum")
        {
            if(cmd == "add")
            {
                cin >> num;

                sumN += num;
                minN = min(num, minN);
                maxN = max(num, maxN);

                check=1; //����� ��, �� �� ������� �������� ���� ��� �������� �����
            }
            else if(check>0)
            {
                if(cmd == "min")
                {
                    cout << minN << endl;
                }
                else if(cmd == "max")
                {
                    cout << maxN << endl;
                }
                else if(cmd == "sum")
                {
                    cout << sumN << endl;
                }
            }
            else
            {
                cout << "No numbers entered\n";
            }
        }
        else
        {
            return 0;
        }
    }
}
