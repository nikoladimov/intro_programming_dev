#include <iostream>
#include <algorithm> //ex2
#include <string> //ex2
#include <math.h> //ex2

using namespace std;

//dummy function to debug main()...

/*Exercise1
int odd_digits(int num)
{
    int result=0;

    while(num>0)
    {
        int last_digit=num%10;
        if(last_digit%2==1)
        {
            result++;
        }
        num/=10;
    }

    return result;
}

int even_digits(int num)
{
    int result=0;

    while(num>0)
    {
        int last_digit=num%10;
        if(last_digit%2==0)
        {
            result++;
        }
        num/=10;
    }

    return result;
}

int main()
{
    int n, num;
    cin>>n;

    for(int i=0; i<n; i++)
    {
        cin>>num;
        cout<<odd_digits(num)<<" "<<even_digits(num)<<endl;
    }

    return 0;
}*/

/*Exercise1a

void check_digit(int num, int& odd, int& even)
{
    while(num>0)
    {
        int last_digit=num%10;
        if(last_digit%2==1)
        {
            odd++;
        }
        else
        {
            even++;
        }
        num/=10;
    }
}

int main()
{
    int n, num;
    cin>>n;

    for(int i=0; i<n; i++)
    {
        int odd=0, even=0;
        cin>>num;

        check_digit(num, odd, even);
        cout<<odd<<" "<<even<<endl;
    }

    return 0;
}*/

//Exercise 2

int bin_to_dec(string num)
{
    int multi=0, result=0;
    reverse(num.begin(), num.end());

    for(int i=0; i<num.length(); i++)
    {
        multi=num[i]-'0';
        result+=multi

        return multi;
    }
}

int main()
{
    string num;

    cin>>num;

    cout<<bin_to_dec(num);
}
