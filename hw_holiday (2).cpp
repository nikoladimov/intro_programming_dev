#include <iostream>

using namespace std;

void swap(double& a, double& b)
{
    double c=a;

    a=b;

    b=c;
}

int main()
{
    double a = 8, b = 168;

    swap(a, b);

    cout << a << " " << b;
}
