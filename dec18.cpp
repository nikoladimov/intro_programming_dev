#include <iostream>
#include <iomanip>

using namespace std;

/*
Exercise 1

string operation, cur1, cur2;
double amount, amount2, total, eu_bg, bg_eu, eu_us, us_eu, bg_us, us_bg;

void set_rate(string cur1, string cur2, double amount)
{
    if(cur1=="EUR" && cur2=="BGN")
    {
        eu_bg=amount;
    }
    else if(cur1=="EUR" && cur2=="USD")
    {
        eu_us=amount;
    }
    else if(cur1=="BGN" && cur2=="EUR")
    {
        bg_eu=amount;
    }
    else if(cur1=="BGN" && cur2=="USD")
    {
        bg_us=amount;
    }
    else if(cur1=="USD" && cur2=="BGN")
    {
        us_bg=amount;
    }
    else if(cur1=="USD" && cur2=="EUR")
    {
        us_eu=amount;
    }
}

double get_rate(string cur1, string cur2)
{
    if(cur1=="EUR" && cur2=="BGN")
    {
        return eu_bg;
    }
    else if(cur1=="EUR" && cur2=="USD")
    {
        return eu_us;
    }
    else if(cur1=="BGN" && cur2=="EUR")
    {
        return bg_eu;
    }
    else if(cur1=="BGN" && cur2=="USD")
    {
        return bg_us;
    }
    else if(cur1=="USD" && cur2=="BGN")
    {
        return us_bg;
    }
    else if(cur1=="USD" && cur2=="EUR")
    {
        return us_eu;
    }
}

int main()
{
    while(cin>>operation)
    {
        if(operation=="set")
        {
            cin>>cur1>>cur2>>amount;
            set_rate(cur1, cur2, amount); //debug + cout
        }
        else if(operation=="convert")
        {
            cin>>cur1>>cur2>>amount2;
            if(get_rate(cur1, cur2)==0)
            {
                cout<<"N.A.";
            }
            else
            {
                cout<<fixed<<setprecision(2)<<get_rate(cur1, cur2)*amount2;
            }
        }
        else if(operation=="rate")
        {
            cin>>cur1>>cur2;
            if(get_rate(cur1, cur2)==0)
            {
                cout<<"N.A.";
            }
            else
            {
                cout<<fixed<<setprecision(6)<<get_rate(cur1, cur2);
            }
        }
    }
}
*/
string arab_to_roman(int num)
{
    string s="";

    while(num>0)
    {
        if(num>1000)
        {
            s+="M";
            num-=1000;
        }
        else if(num>500)
        {
            s+="D";
            num-=500;
        }
        else if(num>100)
        {
            s+="C";
            num-=100;
        }
        else if(num>50)
        {
            s+="L";
            num-=50;
        }
        else if(num>10)
        {
            s+="X";
            num-=10;
        }
        else if(num>5)
        {
            s+="V";
            num-=5;
        }
        else if(num>=1)
        {
            s+="I";
            num-=1;
        }
    }

    return s;
}

int main()
{
    int num;

    while(cin>>num)
    {
        if(num<1)
        {
            cout<<"N.A.";
        }
        else
        {
            cout<<arab_to_roman(num);
        }
    }
}
