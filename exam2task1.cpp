#include <iostream>

using namespace std;

int main()
{
    int num_cin;
    double num;
    double minN=2000000;
    double maxN=-2000000;

    cin >> num_cin;

    if(num_cin>0)
    {
        for(int i=1; i<=num_cin; i++)
        {
            cin >> num;

            if(num>maxN)
            {
                maxN=num;
            }
            if(num<minN)
            {
                minN=num;
            }
        }

        cout << minN;
    }
    else
    {
        return 0;
    }
}
