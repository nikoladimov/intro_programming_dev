//Exercise 3
//Faculty Number: F75329
//Author: Nikola Dimov

#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

int main ()
{
    float wage, hours, salary, overtime;
    const int WEEK_HOURS = 168;
    const int WEEK_WORK = 40;

    string name;

    cout << "Enter employee: ";
    cin >> name;

    cout << "Enter his hourly wage: ";
    cin >> wage;

    if (wage<0)
    {
        cout << "You can't have negative salary!";
    }
    else if (wage==0)
    {
        cout << "You won't receive any salary this week!";
    }
    else
    {
        cout << "Enter how many hours the employee has worked last week: ";
        cin >> hours;

        if(hours>WEEK_HOURS)
        {
            cout << "The week can't contains more than 168 hours!";
        }
        else if(hours<0)
        {
            cout << "It is impossible to work for a negative time!";
        }
        else if(hours>WEEK_WORK)
        {
            overtime=hours-WEEK_WORK;
            salary=wage*hours+wage*0.5*overtime;

            cout << fixed << setprecision(2);
            cout << "The salary of " << name << " for the last week is " << salary << " BGN." << endl;
        }
        else
        {
            salary=wage*hours;

            cout << fixed << setprecision(2);
            cout << "The salary of " << name << " for the last week is " << salary << " BGN." << endl;
        }
    }

    return 0;
}
